package ejercicio3;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		
		
		
		System.out.println("Dame un numero");
		int numero=input.nextInt();
		
		if (numero<30) {
			System.out.println("El numero: "+numero+" es menor de 30");
		}else {
			System.out.println("El numero: "+numero+" es mayor de 30");
		}
		
		input.close();

	}

}
